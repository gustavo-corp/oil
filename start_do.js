const express = require('express')
const app = express()
const port = 3000
var tareas = [{
  id: 1
},{
  oohoho: 1
}]
app.set('view engine','jade');
app.get('/',function(req,res)
{
  res.status(200).json(tareas)
});

app.post('/', function (req, res) {
  res.send('Got a POST request')
})

app.put('/user', function (req, res) {
  res.send('Got a PUT request at /user')
})

app.delete('/user', function (req, res) {
  res.send('Got a DELETE request at /user')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))